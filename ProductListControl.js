//Slick function
$(document).ready(function () {
    $('.Responsive').slick({
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 6,
        slidesToScroll: 5,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: false
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
});

//Mass delete function
const DLT = document.getElementById('delete-product-btn');
DLT.addEventListener('click', (e) => {
    var id = [];
    $(':checkbox:checked').each(function (i) {
        id[i] = $(this).val();
    });

    $.ajax({
        url: 'delete.php',
        method: 'POST',
        data: { id: id },
        success: function () {
            for (var i = 0; i < id.length; i++) {
                $('div#' + id[i] + '').fadeOut('slow');
            }
        }
    });
});