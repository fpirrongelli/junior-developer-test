//Form Validation
var productShow;
var error;
const INPUTS = document.querySelectorAll('#product_form input');
const INPUTD = document.querySelectorAll('#select_product_form input');


INPUTS.forEach((input) => {

    input.addEventListener('blur', (e) => {
        e.preventDefault();
        $(".SkuError").hide();
        $(".FieldError").hide();
        $(".SelectError").hide();
        $(".ZeroError").hide();
        checkInputs(input);
    });
    input.addEventListener('keyup', (e) => {
        e.preventDefault();
        $(".SkuError").hide();
        $(".FieldError").hide();
        $(".SelectError").hide();
        $(".ZeroError").hide();
        checkInputs(input);
    });

});


function checkInputs(id) {
    const VALUE = id.value.trim();
    if (VALUE == '') {
        setErrorFor(id, "Please, fill in the field correctly.");
    }
    else {
        setSuccesFor(id);
    }
}

function setErrorFor(input, message) {
    const FORM_CONTROL = input.parentElement;
    const small = FORM_CONTROL.querySelector('small');
    small.innerText = message;
    FORM_CONTROL.className = 'FormControl Error';
    error = true;
}

function setSuccesFor(input) {
    const FORM_CONTROL = input.parentElement;
    FORM_CONTROL.className = 'FormControl Success';
    error = false;
}

$("#productType").change(function () {
    
    $(".SelectError").hide();
    $(".FieldError").hide();
    INPUTD.forEach((input) => {
        input.value = '';
        input.parentElement.className = 'FormData';
    });

    $(".Data").hide();
    $("#" + $(this).val()).fadeIn(300);
    productShow = $(this).val();


    const TYPE = document.querySelectorAll('#' + productShow + ' input');
    TYPE.forEach((input) => {
           input.parentElement.className = 'FormControl';
    });


});


//Submit Data
const SAVE = document.getElementById('save-product-btn');
SAVE.addEventListener('click', (e) => {
    e.preventDefault();
    if ($("#productType").val() == '') {
        error = true;
        $(".SelectError").show();
    } 

    INPUTS.forEach((input) => {
        const FORM_CONTROL = input.parentElement;
        if (FORM_CONTROL.className == 'FormControl Error' || FORM_CONTROL.className == 'FormControl') {
            setErrorFor(input, "Please, fill in this field.");
            $(".FieldError").show();
        }
    });

    if (error == false) {
        
        INPUTS.forEach((input) => {
            if (input.value.trim() == '') {
                input.value = '0';
            }
        });
        
         var sku = $("#sku").val();
         var name = $("#name").val();
         var price = $("#price").val();
         var size = $("#size").val();
         var weight = $("#weight").val();
         var height = $("#height").val();
         var width = $("#width").val();
         var length = $("#length").val();
        
        if (size == '0'&& weight == '0'&& height == '0'&& width == '0'&& length == '0'){
            error = true;
            $(".ZeroError").show();
        } else{
             $.ajax({
             url: 'form.php',
             method: 'POST',
             data: {
                 sku: sku,
                 name: name,
                 price: price,
                 size: size,
                 weight: weight,
                 height: height,
                 width: width,
                 length: length
             },
             success: function (r) {
                 if (r == 1) {
                     window.location.assign('index.php');
                 }
                 else {
                     error = true;
                     $(".SkuError").show();
                 }
             }
         });
        }
    } else {
        error = true;
    }

});

