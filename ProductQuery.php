<?php
include 'conn.php';
$counter = 0;

$sql = "SELECT sku, name, price, size, weight, height, width, length FROM products";
$result = $conn->query($sql);
if ($result->num_rows > 0) {

    while($row = $result->fetch_assoc()) {
        if($counter == 0){
            echo "<div class='Responsive'>";
          $counter= 1;
        }
        $counter= $counter + 1;
        echo "<div class='Block' id='". $row["sku"]."'><input type='checkbox' autocomplete='off' class='delete-checkbox' name='delete-checkbox' value='". 
        $row["sku"]."'><br><br><small>" . $row["sku"]. "</small><br><small>" . $row["name"] . "</small><br><small>". $row["price"]. " $</small>";
        if($row["size"]!=='0'){
            echo "<br><small>Size: ".$row["size"]." MB</small></div>";
        }
        if($row["weight"]!=='0'){
           echo "<br><small>Weight: ".$row["weight"]." KG</small></div>";
        }
        if($row["height"]!=='0' || $row["width"]!=='0' || $row["length"]!=='0' ){
            echo "<br><small>Dimensions: ".$row["height"]."x".$row["width"]."x".$row["length"]."</small></div>";
        }
        if(($counter/6) == 1){
            echo "</div>";
            $counter= 0;
        }
    }
    echo "</div>";
} else { echo "<div class='Block Info'><small>The database is currently empty. Please add new products.</small></div>"; }
?>